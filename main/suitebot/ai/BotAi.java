package main.suitebot.ai;

import main.suitebot.game.GameState;
import main.suitebot.game.Move;

public interface BotAi
{
    int height=0;
    int width=0; 
    /**
     * Returns the move that the AI intends to play.
     *
     * @param botId ID of the bot operated by the AI
     * @param gameState current game state
     * @return the move that the AI intends to play
     */
    Move makeMove(int botId, GameState gameState){
        int x=0,y=0; 
        Point currentLocation= gameState.getBotLocation(botId);
        Set<Point> obstacles= new Set<Point>();
        
        height= gameState.getPlanHeight();
        width=gameState.getPlanWidth();
        
        obstacles= gameState.getObstacleLocation();
        
        //Boundaries (0, width-1)(0,height-1)
        //check 2 steps up
        boolean decision= true;
        Move move;
        Point up= new Point(currentLocation.x, currentLocation.y-1);
        Point left= new Point(currentLocation.x-1, currentLocation.y);
        Point right=new Point(currentLocation.x, currentLocation.y+1);
        Point down= new Point((currentLocation.x+1, currentLocation.y);
        Point dUp= new Point(currentLocation.x, currentLocation.y-2);
        Point dLeft= new Point(currentLocation.x-2, currentLocation.y); 
        Point dRight=new Point(currentLocation.x, currentLocation.y+2);
        Point dDown= new Point(currentLocation.x+2, currentLocation.y);
       while(decision){
            if(!(obstacles.contains(ceiling(up)) && obstacles.contains(ceiling(dUp)))){
                move= move.UP_;
                decision= false;
            }
            else if(!(obstacles.contains(ceiling(left)) && obstacles.contains(ceiling(dLeft)))){
                move=move.LEFT_;
                decision=false;
            }
            else if(!(obstacles.contains(ceiling(right)) && obstacles.contains(ceiling(dRight)))){
                move=move.RIGHT_;
                decision= false;
            }
            else if(!(obstacles.contains(ceiling(down)) && obstacles.contains(ceiling(dDown)))){
                move=move.DOWN_;
                decision= false;
            }
            else if(!(obstacles.contains(ceiling(up)))){
                move=move.UP_;
                decision=false;
            }
            else if(!(obstacles.contains(ceiling(left)))){
                move=move.LEFT_;
                decision=false;
            }
            else if(!(obstacles.contains(ceiling(right)))){
                move=move.RIGHT_;
                decision=false;
            }
            else if(!(obstacles.contains(ceiling(down)))){
                move=move.DOWN_;
                decision=false;
            }
            /*if(obstacles.contains(new Point((currentLocation.x+2)%width, currentLocation.y+1%height))){
                
            }*/
       }
       return move;
    };
    Point ceiling(Point adjusted){
        Point move;
        if(adjusted.y==height){
            adjusted.y=0;                                                
        }
        if(adjusted.x==width){
            adjusted.x=0;
        }
        if(adjusted.y<0){
            adjusted.y=height-1;
        }
        if(adjusted.x<0){
            adjusted.x=width-1;
        }
        return move= new Point(adjusted.x, adjusted.y); 
    }

    /**
     * Returns the name of the bot.
     *
     * @return the name of the bot
     */
    String getName(){
        return "Alfred PennyPacker";
    };
}
