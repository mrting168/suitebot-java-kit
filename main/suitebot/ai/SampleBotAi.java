package main.suitebot.ai;

import com.google.common.collect.ImmutableList;
import suitebot.game.GameState;
import suitebot.game.Move;
import suitebot.game.Point;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 * Sample AI. The AI has some serious flaws, which is intentional.
 */
public class SampleBotAi implements BotAi
{
	private static final List<Move> SINGLE_MOVES = ImmutableList.of(
			Move.UP_, Move.DOWN_, Move.LEFT_, Move.RIGHT_);

	private static final List<Move> DOUBLE_MOVES = ImmutableList.of(
			Move.UP_UP, Move.UP_DOWN, Move.UP_LEFT, Move.UP_RIGHT,
			Move.DOWN_UP, Move.DOWN_DOWN, Move.DOWN_LEFT, Move.DOWN_RIGHT,
			Move.LEFT_UP, Move.LEFT_DOWN, Move.LEFT_LEFT, Move.LEFT_RIGHT,
			Move.RIGHT_UP, Move.RIGHT_DOWN, Move.RIGHT_LEFT, Move.RIGHT_RIGHT);

	private int botId;
	private GameState gameState;

	private final Predicate<Move> isMoveToTreasure = move -> gameState.getTreasureLocations().contains(destination(move));
	private final Predicate<Move> isMoveToBattery = move -> gameState.getBatteryLocations().contains(destination(move));
	private final Predicate<Move> isSafeMove = move -> !gameState.getObstacleLocations().contains(destination(move));

	private final Supplier<Stream<Move>> closeTreasureMoveSupplier = () -> SINGLE_MOVES.stream().filter(isMoveToTreasure);
	private final Supplier<Stream<Move>> closeBatteryMoveSupplier = () -> SINGLE_MOVES.stream().filter(isMoveToBattery);
	private final Supplier<Stream<Move>> reachableTreasureMoveSupplier = () -> DOUBLE_MOVES.stream().filter(isMoveToTreasure);
	private final Supplier<Stream<Move>> reachableBatteryMoveSupplier = () -> DOUBLE_MOVES.stream().filter(isMoveToBattery);
	private final Supplier<Stream<Move>> safeMoveSupplier = () -> SINGLE_MOVES.stream().filter(isSafeMove);

	/**
	 * If a treasure is close (distance 1), go to it;
	 * otherwise, if a battery is close, go to it;
	 * otherwise, if a treasure is reachable (distance 2), go to it;
	 * otherwise, if a battery is reachable, go to it;
	 * otherwise, if a safe move can be made (one that avoids any obstacles), do it;
	 * otherwise, go down.
	 */
	@Override
	public Move makeMove(int botId, GameState gameState)
	{
		this.botId = botId;
		this.gameState = gameState;

		if (isDead())
			return null;

		return Stream.of(
				closeTreasureMoveSupplier,
				closeBatteryMoveSupplier,
				reachableTreasureMoveSupplier,
				reachableBatteryMoveSupplier,
				safeMoveSupplier
		)
				.map(Supplier::get)
				.map(Stream::findFirst)
				.filter(Optional::isPresent)
				.map(Optional::get)
				.findFirst()
				.orElse(Move.DOWN_);
	}

	private boolean isDead()
	{
		return !gameState.getLiveBotIds().contains(botId);
	}

	private Point destination(Move move)
	{
		Point botLocation = gameState.getBotLocation(botId);
		Point step1Destination = move.step1.from(botLocation);

		if (move.step2 == null)
			return step1Destination;
		else
			return move.step2.from(step1Destination);
	}

	@Override
	public String getName()
	{
		return "Sample AI";
	}
}
