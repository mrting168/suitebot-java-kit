package main.suitebot.server;

public interface SimpleRequestHandler
{
	String processRequest(String request);
}
