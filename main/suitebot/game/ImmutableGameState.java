package main.suitebot.game;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ImmutableGameState implements GameState
{
	private final int planWidth;
	private final int planHeight;
	private final ImmutableList<Integer> botIds;
	private final ImmutableSet<Integer> liveBotIds;
	private final ImmutableMap<Integer, Point> botLocationMap;
	private final ImmutableMap<Integer, Integer> botEnergyMap;
	private final ImmutableSet<Point> obstacles;
	private final ImmutableSet<Point> treasures;
	private final ImmutableSet<Point> batteries;

	@Override
	public int getPlanWidth()
	{
		return planWidth;
	}

	@Override
	public int getPlanHeight()
	{
		return planHeight;
	}

	@Override
	public List<Integer> getAllBotIds()
	{
		return botIds;
	}

	@Override
	public Set<Integer> getLiveBotIds()
	{
		return liveBotIds;
	}

	@Override
	public Point getBotLocation(int botId)
	{
		Point location = botLocationMap.get(botId);
		if (location == null)
			assertKnownBotId(botId);
		return location;
	}

	@Override
	public int getBotEnergy(int botId)
	{
		Integer energy = botEnergyMap.get(botId);
		if (energy == null)
		{
			assertKnownBotId(botId);
			return 0;
		}
		return energy;
	}

	@Override
	public Set<Point> getObstacleLocations()
	{
		return obstacles;
	}

	@Override
	public Set<Point> getTreasureLocations()
	{
		return treasures;
	}

	@Override
	public Set<Point> getBatteryLocations()
	{
		return batteries;
	}

	public static Builder builder()
	{
		return new Builder();
	}

	public static Builder builder(GameState gameState)
	{
		Map<Integer, Point> botLocationMap = gameState.getAllBotIds().stream()
				.filter(botId -> gameState.getBotLocation(botId) != null)
				.collect(Collectors.toMap(Function.identity(), gameState::getBotLocation));

		Map<Integer, Integer> botEnergyMap = gameState.getAllBotIds().stream()
				.collect(Collectors.toMap(Function.identity(), gameState::getBotEnergy));

		return new Builder()
				.setPlanWidth(gameState.getPlanWidth())
				.setPlanHeight(gameState.getPlanHeight())
				.setBotIds(gameState.getAllBotIds())
				.setLiveBotIds(gameState.getLiveBotIds())
				.setBotLocationMap(botLocationMap)
				.setBotEnergyMap(botEnergyMap)
				.setObstacles(gameState.getObstacleLocations())
				.setBatteries(gameState.getBatteryLocations())
				.setTreasures(gameState.getTreasureLocations());
	}

	private ImmutableGameState(Builder builder)
	{
		preBuildValidation(builder);

		this.planWidth = builder.planWidth;
		this.planHeight = builder.planHeight;
		this.botIds = ImmutableList.copyOf(builder.botIds);
		this.botLocationMap = ImmutableMap.copyOf(builder.botLocationMap);
		this.botEnergyMap = ImmutableMap.copyOf(builder.botEnergyMap);

		this.liveBotIds = java.util.Optional.ofNullable(builder.liveBotIds).map(ImmutableSet::copyOf).orElse(ImmutableSet.copyOf(builder.botIds));
		this.obstacles = java.util.Optional.ofNullable(builder.obstacles).map(ImmutableSet::copyOf).orElse(ImmutableSet.of());
		this.treasures = java.util.Optional.ofNullable(builder.treasures).map(ImmutableSet::copyOf).orElse(ImmutableSet.of());
		this.batteries = java.util.Optional.ofNullable(builder.batteries).map(ImmutableSet::copyOf).orElse(ImmutableSet.of());

		postBuildValidation();
	}

	private static void preBuildValidation(Builder builder)
	{
		assertBuildable(builder.botIds != null, "botIds are mandatory");
		assertBuildable(builder.botLocationMap != null, "botLocationMap is mandatory");
		assertBuildable(builder.botEnergyMap != null, "botEnergyMap is mandatory");
	}

	private void postBuildValidation()
	{
		assertBuildable(planWidth > 0, "planWidth must be positive");
		assertBuildable(planHeight > 0, "planHeight must be positive");
		assertBuildable(hasUniqueValues(botIds), "duplicate values in botIds");
		assertBuildable(hasUniqueValues(botLocationMap.values()), "duplicate bot locations");
		assertLocationSetForAllLiveBots();
		assertEnergySetForAllLiveBots();
		assertMultipleObjectsDoNotOccupyTheSameLocation();
	}

	private void assertLocationSetForAllLiveBots()
	{
		for (int botId : liveBotIds)
			if (botLocationMap.get(botId) == null)
				throw new UnableToBuildException("location not set for the bot " + botId);
	}

	private void assertEnergySetForAllLiveBots()
	{
		for (int botId : liveBotIds)
			if (botEnergyMap.get(botId) == null)
				throw new UnableToBuildException("energy not set for the bot " + botId);
	}

	private void assertMultipleObjectsDoNotOccupyTheSameLocation()
	{
		List<Point> allLocations = new ArrayList<>();
		allLocations.addAll(botLocationMap.values());
		allLocations.addAll(obstacles);
		allLocations.addAll(treasures);
		allLocations.addAll(batteries);
		assertBuildable(hasUniqueValues(allLocations), "multiple objects may not occupy the same location");
	}

	private static void assertBuildable(boolean assertion, String message) throws UnableToBuildException
	{
		if (!assertion)
			throw new UnableToBuildException(message);
	}

	private static <T> boolean hasUniqueValues(Collection<T> collection)
	{
		return collection.size() == ImmutableSet.copyOf(collection).size();
	}

	private void assertKnownBotId(int botId)
	{
		if (!botIds.contains(botId))
			throw new IllegalArgumentException("unknown bot ID: " + botId);
	}

	public static class Builder
	{
		private int planWidth;
		private int planHeight;
		private Iterable<Integer> botIds;
		private Iterable<Integer> liveBotIds;
		private Map<Integer, Point> botLocationMap;
		private Map<Integer, Integer> botEnergyMap;
		private Iterable<Point> obstacles;
		private Iterable<Point> treasures;
		private Iterable<Point> batteries;

		public Builder setPlanWidth(int planWidth)
		{
			this.planWidth = planWidth;
			return this;
		}

		public Builder setPlanHeight(int planHeight)
		{
			this.planHeight = planHeight;
			return this;
		}

		public Builder setBotIds(Iterable<Integer> botIds)
		{
			this.botIds = botIds;
			return this;
		}

		public Builder setLiveBotIds(Iterable<Integer> liveBotIds)
		{
			this.liveBotIds = liveBotIds;
			return this;
		}

		public Builder setBotLocationMap(Map<Integer, Point> botLocationMap)
		{
			this.botLocationMap = botLocationMap;
			return this;
		}

		public Builder setBotEnergyMap(Map<Integer, Integer> botEnergyMap)
		{
			this.botEnergyMap = botEnergyMap;
			return this;
		}

		public Builder setObstacles(Iterable<Point> obstacles)
		{
			this.obstacles = obstacles;
			return this;
		}

		public Builder setTreasures(Iterable<Point> treasures)
		{
			this.treasures = treasures;
			return this;
		}

		public Builder setBatteries(Iterable<Point> batteries)
		{
			this.batteries = batteries;
			return this;
		}

		public ImmutableGameState build()
		{
			return new ImmutableGameState(this);
		}
	}

	public static class UnableToBuildException extends RuntimeException
	{
		public UnableToBuildException(String message)
		{
			super(message);
		}
	}
}
