package main.suitebot;

import main.suitebot.ai.BotAi;
import main.suitebot.game.GameState;
import main.suitebot.game.Move;
import main.suitebot.json.JsonUtil;
import main.suitebot.server.SimpleRequestHandler;

public class BotRequestHandler implements SimpleRequestHandler
{
	public static final String NAME_REQUEST = "NAME";

	private final BotAi botAi;

	public BotRequestHandler(BotAi botAi)
	{
		this.botAi = botAi;
	}

	@Override
	public String processRequest(String request)
	{
		try
		{
			return processRequestInternal(request);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return e.toString();
		}
	}

	private String processRequestInternal(String request)
	{
		if (NAME_REQUEST.equals(request))
			return botAi.getName();

		return processMoveRequest(request);
	}

	private String processMoveRequest(String request)
	{
		int botId = JsonUtil.deserializeYourBotId(request);
		GameState gameState = JsonUtil.deserializeGameState(request);
		Move move = botAi.makeMove(botId, gameState);
		return move != null ? move.toString() : null;
	}
}
