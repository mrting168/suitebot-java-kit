package test.suitebot;

import org.junit.Before;
import org.junit.Test;
import suitebot.ai.BotAi;
import suitebot.game.Direction;
import suitebot.game.GameState;
import suitebot.game.Move;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class BotRequestHandlerTest
{
	private static final Move AI_MOVE = new Move(Direction.LEFT);
	private static final String AI_NAME = "My AI";

	private BotRequestHandler REQUEST_HANDLER;

	@Before
	public void setUp() throws Exception
	{
		BotAi botAi = new BotAi()
		{
			@Override
			public Move makeMove(int botId, GameState gameState)
			{
				return AI_MOVE;
			}

			@Override
			public String getName()
			{
				return AI_NAME;
			}
		};

		REQUEST_HANDLER = new BotRequestHandler(botAi);
	}

	@Test
	public void testNameRequest() throws Exception
	{
		assertThat(REQUEST_HANDLER.processRequest(BotRequestHandler.NAME_REQUEST), equalTo(AI_NAME));
	}
}