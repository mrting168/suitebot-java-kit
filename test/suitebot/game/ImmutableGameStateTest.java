package suitebot.game;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

public class ImmutableGameStateTest
{
	private static final int PLAN_WIDTH = 12;
	private static final int PLAN_HEIGHT = 12;
	private static final List<Integer> BOT_IDS = ImmutableList.of(1, 2, 3);
	private static final List<Integer> LIVE_BOT_IDS = ImmutableList.of(1, 2);
	private static final Map<Integer, Point> BOT_LOCATIONS = ImmutableMap.of(
			1, new Point(0, 0),
			2, new Point(11, 11));
	private static final Map<Integer, Integer> BOT_ENERGY = ImmutableMap.of(
			1, 20,
			2, 25);
	private static final List<Point> OBSTACLES = ImmutableList.of(new Point(1, 1), new Point(2, 2));
	private static final List<Point> BATTERIES = ImmutableList.of(new Point(3, 3), new Point(4, 4));
	private static final List<Point> TREASURES = ImmutableList.of(new Point(4, 5), new Point(6, 7));

	private GameState gameState;

	@Before
	public void setUp() throws Exception
	{
		gameState = ImmutableGameState.builder()
				.setPlanWidth(PLAN_WIDTH)
				.setPlanHeight(PLAN_HEIGHT)
				.setBotIds(BOT_IDS)
				.setLiveBotIds(LIVE_BOT_IDS)
				.setBotLocationMap(BOT_LOCATIONS)
				.setBotEnergyMap(BOT_ENERGY)
				.setObstacles(OBSTACLES)
				.setBatteries(BATTERIES)
				.setTreasures(TREASURES)
				.build();
	}

	@Test
	public void getBotLocation_onDeadBotId_shouldReturnNull() throws Exception
	{
		assertThat(gameState.getBotLocation(3), is(nullValue()));
	}

	@Test(expected = IllegalArgumentException.class)
	public void getBotLocation_onUnknownBotId_shouldThrowException() throws Exception
	{
		gameState.getBotLocation(4);
	}

	@Test
	public void getBotEnergy_onDeadBotId_shouldReturnSomeValue() throws Exception
	{
		assertThat(gameState.getBotEnergy(3), is(not(nullValue())));
	}

	@Test(expected = IllegalArgumentException.class)
	public void getBotEnergy_onUnknownBotId_shouldThrowException() throws Exception
	{
		gameState.getBotLocation(5);
	}
}
